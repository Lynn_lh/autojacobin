Auto-JacoBin. Version 1.1
Xiping Fu
fxpfxp0607@gmail.com
--------------------------------------------------
demo.m shows an example of the proposed method. The trainAutoJacoBin.m is used for learning the hashing functions. The encodeAutoJacoBin.m is used for encoding the data points.
For the optimization, Mark Schmidt's optimization toolbox is used:



If you use this toolbox in your work, we would be grateful if you cite:
@article{fu2016Auto-JacoBin,
title={Auto-JacoBin: Auto-encoder Jacobian Binary Hashing},
author={Fu, Xiping and McCane, Brendan and Mills, Steven and Albert, Michael and Lech Szymanski},
journal={submitted to PAMI}
}