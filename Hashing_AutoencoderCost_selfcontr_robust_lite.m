function [cost,grad] = Hashing_AutoencoderCost_selfcontr_robust_lite(theta, visibleSize,hiddenSize1,auto, contr, beta,Ts, data)

% visibleSize: the number of input units
% hiddenSize1: the number of hidden units
% auto: the weight paramter of the autoencoder model
% contr: used for the first order approximation
% beta: for the bit diversation
% Ts: the estimated tangent planes
% data: no.sample*dimension

%minizing object: min auto*||z3-x||_F^2+contr||J-I||_F^2 + beta||z2z2'-NI||_1    

theta_stable=theta;
epsion=0.0001; %used for approximate the L1 norm 
%convert the theta vector into parameters
W1 = reshape(theta(1:hiddenSize1*visibleSize), hiddenSize1, visibleSize);
theta(1:hiddenSize1*visibleSize)=[];
W2 = reshape(theta(1:hiddenSize1*visibleSize), visibleSize, hiddenSize1);
theta(1:hiddenSize1*visibleSize)=[];
b1 = theta(1:hiddenSize1);
theta(1:hiddenSize1)=[];
b2 = theta(1:visibleSize);
theta(1:visibleSize)=[];

% Cost and gradient variables 
cost = 0;
W1grad = zeros(size(W1)); 
W2grad = zeros(size(W2)); 
b1grad = zeros(size(b1)); 
b2grad = zeros(size(b2));

%prepare the data points through the newwork
mm=size(data,2);
z2=sigmoid_tanh((W1*data)+repmat(b1,1,mm));
z2_tangh_at=sigmoidGradient_tanh_at(z2);
z3=sigmoid_tanh((W2*z2)+repmat(b2,1,mm));
z3_tangh_at=sigmoidGradient_tanh_at(z3);

x=data();
clear data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%approximate ||YY'-NI||_1
G=z2*z2'-mm*eye(hiddenSize1);
G_grad=G.*(G.^2+epsion).^(-0.5);
deltaY1=(auto*W2'*((2*(z3-x)).*z3_tangh_at)+beta*(G_grad*z2+G_grad'*z2)).*z2_tangh_at;

%using ||Y-B||_F^2
deltaY2=(2*(z3-x)).*z3_tangh_at;

b1grad=b1grad+deltaY1*ones(mm,1);
b2grad=b2grad+auto*deltaY2*ones(mm,1);
W1grad=W1grad+deltaY1*x';
W2grad=W2grad+auto*deltaY2*z2';

%-------------------------------------------------------------------
% return the grad and the cost
grad = [W1grad(:) ;W2grad(:) ; b1grad(:) ; b2grad(:)];
grad=grad/mm;
cost=auto*sum(sum((z3-x).^2))+beta*sum(sum(((z2*z2'-1*mm*eye(hiddenSize1)).^2+epsion).^0.5));
cost=cost/mm;

if contr>0 & size(Ts,3)>0
[cost_stable,grad_stable] = Gradient_cost_stable(x(:,1:size(Ts,3)), visibleSize,hiddenSize1, contr,theta_stable,Ts);
grad=grad+grad_stable;
cost1=cost
cost2=cost_stable
cost=cost+cost_stable;
end
end

function sigm = sigmoid_tanh(x)
  
    sigm = (exp(x) - exp(-x)) ./ (exp(x) + exp(-x));
end

function g = sigmoidGradient_tanh(z)
g=zeros(size(z));
g=(1+sigmoid_tanh(z)).*(1-sigmoid_tanh(z));
end

function g = sigmoidGradient_tanh_at(z)
g=zeros(size(z));
g=(1+z).*(1-z);
end

function [cost_stable,grad_stable] = Gradient_cost_stable(x, visibleSize,hiddenSize1, contr,theta,As)
%convert the theta vector into parameters
W1 = reshape(theta(1:hiddenSize1*visibleSize), hiddenSize1, visibleSize);
theta(1:hiddenSize1*visibleSize)=[];
W2 = reshape(theta(1:hiddenSize1*visibleSize), visibleSize, hiddenSize1);
theta(1:hiddenSize1*visibleSize)=[];
b1 = theta(1:hiddenSize1);
theta(1:hiddenSize1)=[];
b2 = theta(1:visibleSize);
theta(1:visibleSize)=[];

% Cost and gradient variables 
cost = 0;
W1grad = zeros(size(W1)); 
W2grad = zeros(size(W2)); 
b1grad = zeros(size(b1)); 
b2grad = zeros(size(b2));

%prepare the data points through the newwork
mm=size(x,2);
z2=sigmoid_tanh((W1*x)+repmat(b1,1,mm));
z2_tangh_at=sigmoidGradient_tanh_at(z2);
z3=sigmoid_tanh((W2*z2)+repmat(b2,1,mm));
z3_tangh_at=sigmoidGradient_tanh_at(z3);

deltaCtangent=permute(repmat(z3_tangh_at,[1,1,hiddenSize1]),[3,1,2]);
deltaCtangent4=permute(repmat(z2_tangh_at,[1,1,visibleSize]),[1,3,2]);
deltaCtangent6=reshape(W1'*(reshape(deltaCtangent4,[hiddenSize1,visibleSize*mm]).*repmat(W2',[1,mm])),[visibleSize visibleSize mm]);
deltaCtangent3=reshape(W1'*reshape((deltaCtangent.*repmat(W2',[1,1,mm])).*deltaCtangent4,[hiddenSize1,visibleSize*mm]),[visibleSize visibleSize mm]);

deltaCtangent7=permute(repmat(z2_tangh_at,[1,1,visibleSize]),[1,3,2]);
deltaCtangent8 = deltaCtangent7.*repmat(W1,[1,1,mm]);
deltaCtangent66=reshape(W1'*(reshape(deltaCtangent4.*(deltaCtangent.^2),[hiddenSize1,visibleSize*mm]).*repmat(W2',[1,mm])),[visibleSize visibleSize mm]);
deltaCtangent9t=reshape(permute(deltaCtangent66,[2,1,3]),[visibleSize visibleSize*mm])*reshape(deltaCtangent8,[hiddenSize1,visibleSize*mm])';
clear deltaCtangent66
deltaCtangent10p=-4*(z3).*z3_tangh_at.^2;
deltaCtangent11p=sum(permute(deltaCtangent6.^2, [2 3 1]),3);
deltaCtangent11=(permute(repmat(deltaCtangent10p,[1,1,hiddenSize1]),[3,1,2]).*repmat(W2',[1,1,mm])).*deltaCtangent4.*...
                permute(repmat(deltaCtangent11p,[1,1,hiddenSize1]),[3,1,2]);
t1=reshape(W1'*(reshape(deltaCtangent.^2,[hiddenSize1,visibleSize*mm]).*repmat(W2',[1,mm]).*reshape(deltaCtangent4,[hiddenSize1,visibleSize*mm])),[visibleSize visibleSize mm]);
deltaCtangent12=reshape(W2'*reshape(permute(t1,[2,1,3]),[visibleSize,visibleSize*mm]),[hiddenSize1 visibleSize mm]).*deltaCtangent7;
%clear t1
deltaCtangent13=deltaCtangent6.*permute(repmat(z3_tangh_at,[1,1,visibleSize]).^2,[3,1,2]);
deltaCtangent14=permute((permute(repmat(W1,[1,1 visibleSize]),[3,1,2]).*repmat(W2,[1,1,visibleSize])),[3,1,2]);
ZZ=reshape(deltaCtangent14,[visibleSize*visibleSize,hiddenSize1])'*reshape(deltaCtangent13,[visibleSize*visibleSize,mm]);
clear deltaCtangent13 deltaCtangent14

%for stable case
deltaStable0=-2*(z3).*z3_tangh_at;
deltaStable1p=permute(repmat(z3_tangh_at,[1,1,visibleSize]),[3,1,2]);
deltaStable1=(reshape(deltaCtangent8,[hiddenSize1 visibleSize*mm])*reshape(permute(As.*deltaStable1p,[2 1 3]),[visibleSize visibleSize*mm])')';
deltaStable2=sum(permute(deltaCtangent6.*As,[2 3 1]),3);
deltaStable3=(permute(repmat(deltaStable0,[1,1,hiddenSize1]),[3,1,2]).*repmat(W2',[1,1,mm])).*deltaCtangent4.*...
                permute(repmat(deltaStable2,[1,1,hiddenSize1]),[3,1,2]);
deltaStable4=reshape(deltaCtangent.*(deltaCtangent4.*repmat(W2',[1,1,mm])),[hiddenSize1 visibleSize*mm])*reshape(As,[visibleSize visibleSize*mm])';  
deltaStable5=As.*deltaStable1p;
deltaStable6=permute((permute(repmat(W1,[1,1 visibleSize]),[3,1,2]).*repmat(W2,[1,1,visibleSize])),[3,1,2]);
deltaStable7=reshape(deltaStable6,[visibleSize*visibleSize,hiddenSize1])'*reshape(deltaStable5,[visibleSize*visibleSize,mm]);

b1grad=b1grad+contr*squeeze(sum(deltaCtangent11,2))*ones(mm,1)+2*contr*(ZZ.*((-2*z2).*(z2_tangh_at)))*ones(mm,1)...
    -2*contr*squeeze(sum(deltaStable3,2))*ones(mm,1)-2*contr*(deltaStable7.*((-2*z2).*(z2_tangh_at)))*ones(mm,1);
b2grad=b2grad+contr*(deltaCtangent11p.*(deltaCtangent10p))*ones(mm,1)+4*contr*(z3.*z3_tangh_at.*sum(permute(deltaCtangent6.*As,[2 3 1]),3))*ones(mm,1);
W1grad=W1grad+contr*squeeze(sum(deltaCtangent11,2))*x'...
    +2*contr*squeeze(sum(deltaCtangent12,3))+2*contr*(ZZ.*((-2*z2).*(z2_tangh_at)))*x'...
    -2*contr*squeeze(sum(deltaStable3,2))*x'-2*contr*sum(deltaStable4,3)-2*contr*(deltaStable7.*((-2*z2).*(z2_tangh_at)))*x';
W2grad=W2grad+contr*(deltaCtangent11p.*(deltaCtangent10p))*z2' ...
        +2*contr*deltaCtangent9t+4*contr*(z3.*z3_tangh_at.*sum(permute(deltaCtangent6.*As,[2 3 1]),3))*z2'-2*contr*deltaStable1;

%-------------------------------------------------------------------
% return the grad and the cost
grad_stable= [W1grad(:) ;W2grad(:) ; b1grad(:) ; b2grad(:)];
grad_stable=grad_stable/mm;
cost_stable=contr*sum(sum(sum((deltaCtangent3-As).^2)));
cost_stable=cost_stable/mm;
end